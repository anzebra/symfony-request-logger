Symfony User requests logger
========================

## Installation

After cloning project, install composer dependencies:

```bash
composer install
```

Create database 
```bash
app/console doctrine:database:create
```

Create database schema
```bash
app/console doctrine:schema:create
```

Fill database with fixtures:
```bash
app/console doctrine:fixtures:load
```

## Crontab command

Use this command to send statistic from the last hour
```bash
app/console app:send-statistics [<sendTo>]
```

Argument 'sendTo' isn't required. In case if argument isn't send command uses email from project parameters: 'statistic_email_to'


Use parameters.yml to set:
- 'statistic_email_from' - Set the From address
- 'statistic_email_to' - Set the To address
