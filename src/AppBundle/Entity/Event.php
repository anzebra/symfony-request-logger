<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 *
 * @ORM\Table(name="event")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventRepository")
 */
class Event
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="session_id", type="string", length=129)
     */
    private $sessionId;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_address", type="string", length=40)
     */
    private $ipAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text")
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="post_parameters", type="array", nullable=true)
     */
    private $postParameters;

    /**
     * @var string
     *
     * @ORM\Column(name="get_parameters", type="array", nullable=true)
     */
    private $getParameters;

    /**
     * @var string
     *
     * @ORM\Column(name="method", type="string", length=10)
     */
    private $method;

    /**
     * @var string
     *
     * @ORM\Column(name="files_data", type="array", nullable=true)
     */
    private $filesData;

    /**
     * @var string
     *
     * @ORM\Column(name="http_referer", type="string", length=255, nullable=true)
     */
    private $httpReferer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * Event constructor.
     */
    public function __construct()
    {
        $this->setCreated(new \DateTime('now'));
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sessionId
     *
     * @param string $sessionId
     * @return Event
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * Get sessionId
     *
     * @return string 
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * Set ipAddress
     *
     * @param string $ipAddress
     * @return Event
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress
     *
     * @return string 
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Event
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set postParameters
     *
     * @param string $postParameters
     * @return Event
     */
    public function setPostParameters($postParameters)
    {
        $this->postParameters = $postParameters;

        return $this;
    }

    /**
     * Get postParameters
     *
     * @return string 
     */
    public function getPostParameters()
    {
        return $this->postParameters;
    }

    /**
     * Set getParameters
     *
     * @param string $getParameters
     * @return Event
     */
    public function setGetParameters($getParameters)
    {
        $this->getParameters = $getParameters;

        return $this;
    }

    /**
     * Get getParameters
     *
     * @return string 
     */
    public function getGetParameters()
    {
        return $this->getParameters;
    }

    /**
     * Set method
     *
     * @param string $method
     * @return Event
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get method
     *
     * @return string 
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set httpReferer
     *
     * @param string $httpReferer
     * @return Event
     */
    public function setHttpReferer($httpReferer)
    {
        $this->httpReferer = $httpReferer;

        return $this;
    }

    /**
     * Get httpReferer
     *
     * @return string 
     */
    public function getHttpReferer()
    {
        return $this->httpReferer;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Event
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFilesData()
    {
        return $this->filesData;
    }

    /**
     * Set fileName
     *
     * @param array $filesData
     * @return $this
     */
    public function setFilesData($filesData)
    {
        $this->filesData = $filesData;

        return $this;
    }
}
