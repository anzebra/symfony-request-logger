<?php

namespace AppBundle\Service;

use AppBundle\Entity\Event;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class RequestLog
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function logRequest(Request $request)
    {
        $event = new Event();
        $event->setSessionId($request->getSession()->getId())
              ->setIpAddress($request->getClientIp())
              ->setUrl($request->getUri())
              ->setPostParameters($request->query->all())
              ->setGetParameters($request->request->all())
              ->setMethod($request->getMethod())
              ->setHttpReferer($request->headers->get('refferer'));

        if ($request->files->count()) {
            $event->setFilesData($this->getFilesData($request));
        }

        $this->em->persist($event);
        $this->em->flush();
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function getFilesData(Request $request)
    {
        $filesList = $request->files->all();

        $filesBag = [];
        foreach ($filesList as $listName => $list) {
            $listBag = [];
            foreach ($list as $itemName => $listItem) {
                if ($listItem instanceof UploadedFile) {
                    $listBag[$itemName] = [
                        'name' => $listItem->getClientOriginalName(),
                        'tmpName' => $listItem->getFilename()
                    ];
                }
            }

            if ($listBag) {
                $filesBag[$listName] = $listBag;
            }
        }

        return $filesBag;
    }
}