<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\TwigBundle\TwigEngine;

class StatisticEmail
{
    /**
     * @var string
     */
    private $sendFrom;
    /**
     * @var
     */
    private $mailer;
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var TwigEngine
     */
    private $templating;

    /**
     * StatisticEmail constructor.
     * @param string $sendFrom
     * @param $mailer
     * @param EntityManager $em
     * @param TwigEngine $templating
     */
    public function __construct($sendFrom, $mailer, EntityManager $em, TwigEngine $templating)
    {
        $this->sendFrom = $sendFrom;
        $this->mailer = $mailer;
        $this->em = $em;
        $this->templating = $templating;
    }

    /**
     * @param string $sendTo
     * @throws \RuntimeException
     */
    public function sendEmail($sendTo)
    {
            $eventRepository = $this->em->getRepository('AppBundle:Event');
            list($timeFrom, $timeTo) = $this->getLasthourTime();

            $message = new \Swift_Message($this->getTitle($timeFrom, $timeTo));
            $message->setFrom($this->sendFrom)
                    ->setTo($sendTo)
                    ->setBody(
                        $this->templating->render(
                            'Emails/statistic.html.twig',
                            [
                                'eventsCount' => $eventRepository->selectEventsFromLastHour(),
                                'timeFrom' => $timeFrom,
                                'timeTo' => $timeTo
                            ]
                        ),
                        'text/html'
                    );

            $this->mailer->send($message);
    }

    /**
     * @return string
     */
    protected function getTitle($timeFrom, $timeTo)
    {
        return 'Events statistic from ' . $timeFrom . ' to ' . $timeTo;
    }

    protected function getLasthourTime()
    {
        $date = new \DateTime();
        $timeFrom = $date->format('Y-m-d H:i');
        $timeTo = $date->modify('-1 hour')->format('Y-m-d H:i');

        return [$timeFrom, $timeTo];

    }
}