<?php

namespace AppBundle\EventListener;

use AppBundle\Service\RequestLog;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class AppRequestListener
{
    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;
    /**
     * @var RequestLog
     */
    private $logger;

    /**
     * AppRequestListener constructor.
     * @param AuthorizationChecker $authorizationChecker
     * @param RequestLog $logger
     */
    public function __construct(AuthorizationChecker $authorizationChecker, RequestLog $logger)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->logger = $logger;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest() || !$this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            return;
        }


        $this->logger->logRequest($event->getRequest());
    }
}