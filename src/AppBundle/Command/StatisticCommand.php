<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class StatisticCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:send-statistics')
            ->setDescription('Sends statistics from last hour')
            ->addArgument(
                'sendTo',
                InputArgument::OPTIONAL,
                'Who do you want to send email?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sendTo = $input->getArgument('sendTo');
        if (!$sendTo) {
            $sendTo = $this->getContainer()->getParameter('statistic_email_to');
        }

        $statisticEmail = $this->getContainer()->get('app.statistic_email');

        try {
            $output->writeln('Start sending email...');
            $statisticEmail->sendEmail($sendTo);
            $output->writeln('Done.');
        } catch (\Exception $e) {
            $output->writeln('Send email error: ' . $e->getMessage());
        }
    }

}
